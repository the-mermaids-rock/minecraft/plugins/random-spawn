package nl.finlaydag33k.randomspawn;

import org.bukkit.plugin.java.JavaPlugin;

import nl.finlaydag33k.randomspawn.listener.LoginListener;

public class Main extends JavaPlugin {
  @Override
  public void onEnable() {
    // Register event handlers
    getServer().getPluginManager().registerEvents(new LoginListener(), this);
  }
}
