package nl.finlaydag33k.randomspawn.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

public class LoginListener implements Listener {
  private static final HashSet<Material> deniedBlocks = new HashSet<>(Arrays.asList(
    Material.LAVA,
    Material.WATER,
    Material.CACTUS,
    Material.FIRE,
    Material.MAGMA_BLOCK,
    Material.SWEET_BERRY_BUSH,
    Material.CAMPFIRE
  ));
  
  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    // Ignore players that have already played before
    if(event.getPlayer().hasPlayedBefore()) return;
    
    // Get a random location to yeet the player to
    Bukkit.getServer().getLogger().info(event.getPlayer().getName() + " joined for the first time... Attempting to send off at random...");
    Location location = getRandomSpawnLocation(event.getPlayer().getWorld());

    // Teleport the player to the location
    event.getPlayer().teleport(location);
  }
  
  public static Location getRandomSpawnLocation(World world) {
    // Get the amount of players so we don't spread out too far
    int players = Bukkit.getOfflinePlayers().length;
    if(players < 5) players = 5;
    
    // Get random base coordinate to place the player at
    int coordX = ThreadLocalRandom.current().nextInt(players) * ThreadLocalRandom.current().nextInt(160);
    int coordZ = ThreadLocalRandom.current().nextInt(players) * ThreadLocalRandom.current().nextInt(160);
    
    // Check for suitable spawn location
    int attempts = 0;
    while(true) {
      // Limit attempts to 10K
      if(attempts >= 10_000) {
        Bukkit.getServer().getLogger().severe("Could not find suitable spawn within 10K attempts, sending to origin!");
        return new Location(world, 0d, 88d, 0d);
      }
      
      // Check if the block is safe
      final int coordY = world.getHighestBlockYAt(coordX, coordZ);
      Location location = new Location(world, coordX, coordY, coordZ);
      if(LoginListener.deniedBlocks.contains(location.getBlock().getType())) {
        coordX = ThreadLocalRandom.current().nextInt(players) * 160;
        coordZ = ThreadLocalRandom.current().nextInt(players) * 160;
        attempts++;
        continue;
      }
      
      // We've found a suitable location
      return new Location(world, coordX, coordY + 2, coordZ);
    }
  }
}
